
from django.conf.urls import patterns, include, url
from django.views.generic.detail import DetailView

from .views import HomePageView
from .models import Chamada

urlpatterns = patterns('',
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'site/(?P<pk>\d+)/', DetailView.as_view(model=Chamada))
)
