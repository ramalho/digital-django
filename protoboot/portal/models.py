from django.db import models

class Chamada(models.Model):
    titulo = models.CharField(max_length=64)
    resumo = models.TextField(max_length=256)
    url = models.URLField()
    destaque = models.BooleanField()
    criacao = models.DateTimeField(auto_now_add=True)
    atualizacao = models.DateTimeField(auto_now=True)
    # XXXX: usar slug
    # slug = models.CharField(max_length=64, default='', edit=False)

    class Meta:
        get_latest_by = 'atualizacao'

    def __unicode__(self):
        return self.titulo


