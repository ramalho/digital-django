from django.views.generic.base import TemplateView

from .models import Chamada

class HomePageView(TemplateView):

    template_name = 'portal/fluid.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        destaque = Chamada.objects.filter(destaque=True).latest()
        context['destaque'] = destaque
        resultado = Chamada.objects.exclude(id=destaque.id).order_by('atualizacao')[:12]
        context['chamadas'] = ((resultado[i:i+3]) for i in range(0, len(resultado), 3))
        return context
